var assert = require('assert');
//calculate = require('../../app/calculate.controller');

exports.addition_should_add_two_numbers = function(done){
  var result = calculate.add(1,1);
  assert.ok(result === 2);
  return done();
};

exports.addition_should_add_big_numbers = function(done){
  var result = calculate.add(9007199254740991, 9007199254740991);
  assert.ok(result === 18014398509481982);
  return done();
};

exports.addition_should_add_big_numbers = function(done){
  var result = calculate.add(9007199254740992, 9007199254740992);
  assert.ok(result === 'Error');
  return done();
};

exports.subtraction_should_subtract_two_numbers = function(done){
  var result = calculate.subtract(2, 1);
  assert.ok(result === 1);
  return done();
};

exports.multiplication_should_multiply_two_numbers = function(done){
  var result = calculate.multiply(2, 3);
  assert.ok(result === 6);
  return done();
};

exports.division_should_divide_two_numbers = function(done){
  var result = calculate.divide(4, 2);
  assert.ok(result === 2);
  return done();
};
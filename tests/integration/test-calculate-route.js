var supertest = require('supertest');
//var app = require('../../app/app.js');

exports.calculation_should_accept_numbers = function(done){
  supertest(app)
    .get('/calculate?a=1&b=1')
    .expect(200)
    .end(done);
};

exports.calculation_should_accept_negative_numbers = function(done){
  supertest(app)
    .get('/calculate?a=-1&b=-1')
    .expect(200)
    .end(done);
};

exports.calculation_should_accept_float_numbers = function(done){
  supertest(app)
    .get('/calculate?a=1.55&b=2.36')
    .expect(200)
    .end(done);
};

exports.calculation_should_reject_any_chars_accept_numbers = function(done){
  supertest(app)
    .get('/calculate?a=string&b=2')
    .expect(400)
    .end(done);
};

exports.calculation_should_produce_error_if_only_one_parameter_defined = function(done){
  supertest(app)
    .get('/calculate?a=&b=2')
    .expect(400)
    .end(done);
};

exports.calculation_should_accept_addition_operator = function(done){
  supertest(app)
    .get('/calculate?operator=addition')
    .expect(200)
    .end(done);
};

exports.calculation_should_accept_subtraction_operator = function(done){
  supertest(app)
    .get('/calculate?operator=subtraction')
    .expect(200)
    .end(done);
};

exports.calculation_should_accept_multiplication_operator = function(done){
  supertest(app)
    .get('/calculate?operator=multiplication')
    .expect(200)
    .end(done);
};

exports.calculation_should_accept_division_operator = function(done){
  supertest(app)
    .get('/calculate?operator=division')
    .expect(200)
    .end(done);
};

exports.calculation_should_reject_invalid_operator = function(done){
  supertest(app)
    .get('/calculate?operator=asdsds')
    .expect(200)
    .end(done);
};

exports.addition_should_respond_with_a_numeric_result = function(done){
  supertest(app)
  .get('/calculate?a=5&b=4&operator=addition')
  .expect(200)
  .end(function(err, response){
    assert.ok(!err);
    assert.ok(typeof response.body.result === 'number');
    return done();
  });
};

exports.subtraction_should_respond_with_a_numeric_result = function(done){
  supertest(app)
  .get('/calculate?a=5&b=4&operator=subtraction')
  .expect(200)
  .end(function(err, response){
    assert.ok(!err);
    assert.ok(typeof response.body.result === 'number');
    return done();
  });
};

exports.multiplication_should_respond_with_a_numeric_result = function(done){
  supertest(app)
  .get('/calculate?a=5&b=4&operator=multiplication')
  .expect(200)
  .end(function(err, response){
    assert.ok(!err);
    assert.ok(typeof response.body.result === 'number');
    return done();
  });
};

exports.division_should_respond_with_a_numeric_result = function(done){
  supertest(app)
  .get('/calculate?a=5&b=4&operator=division')
  .expect(200)
  .end(function(err, response){
    assert.ok(!err);
    assert.ok(typeof response.body.result === 'number');
    return done();
  });
};
